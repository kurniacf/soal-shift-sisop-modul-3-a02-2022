#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


pthread_t tid[2]; 
pthread_t tid1[2];
pthread_t tid2[2]; 


pid_t child;


int main(){
    int i=0, j=0, k=0;
    int err;
    int status;

    char *folder[] = {"quote","music","hasil"};

    if(fork() == 0){
		char *argv[] = {"mkdir", "-p", "hasil", NULL};	
		execv("/usr/bin/mkdir", argv);
	}
	while(wait(&status) > 0);


while(i < 2) 
	{
		err=pthread_create(&(tid[i]),NULL,&download_unzip,NULL); 
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

while (j < 2)
    {
        err = pthread_create(&(tid1[j]), NULL, &thread_decode, NULL);
        j++;
    }
    pthread_join(tid1[0], NULL);
    pthread_join(tid1[1], NULL);

    if(fork() == 0){
    char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    while(wait(&status) > 0);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
   while(wait(&status) > 0);

   if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestamanda", "-r", "hasil", "hasil",NULL}; 
		execv("/usr/bin/zip", argv);
	}while(wait(&status) > 0);
	
	for(int i = 0; i < 2; i++){	
	if(fork() == 0){
		char *argv[] = {"rm", "-rf", folder[i], NULL};
		execv("/usr/bin/rm", argv);
	}
	}

    while (k < 2)
    {
        err = pthread_create(&(tid2[k]), NULL, &notxt, NULL);
        k++;
    }
    pthread_join(tid2[0], NULL);
    pthread_join(tid2[1], NULL);

    if(fork() == 0){
    char *argv[] = {"mv", "no.txt", "hasil/no.txt", NULL};
        execv("/usr/bin/mv", argv);
    }

    if(fork()==0){
        char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestamanda", "hasil/", "no.txt", NULL}; 
        execv("/usr/bin/zip", args);
        }while(wait(&status) > 0);

    if(fork() == 0){
		char *argv[] = {"rm", "-rf", folder[i], NULL};
		execv("/usr/bin/rm", argv);
	}
	exit(0);
	return 0;
}


void* download_unzip(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	int iter, status;

    char *url[] = {"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
		           "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
	
    char *fileName[] = {"quote.zip","music.zip"};

    char *folder[] = {"quote","music"};

    if(pthread_equal(id, tid[0])){
		if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[0], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[0], "-O", fileName[0], NULL};
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[0], "-d", "/home/amanda/quote", NULL};	
			execv("/usr/bin/unzip", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[0], NULL};
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);

    }else if(pthread_equal(id, tid[1])){
         if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[1], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[1], "-O", fileName[1], NULL};		
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[1], "-d", "/home/amanda/music", NULL};
			execv("/usr/bin/unzip", argv);
		}	
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[1], NULL};	
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);
    }
    return NULL;
}

char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char *base64_decode(char *encoded)
{
    char counts = 0;
    char buffer[4];
    char *decoded = malloc(strlen(encoded) * 3 / 4);
    int i = 0, p = 0;

    for (i = 0; encoded[i] != '\0'; i++)
    {
        char k;
        for (k = 0; k < 64 && base64_map[k] != encoded[i]; k++)
            ;
        buffer[counts++] = k;
        if (counts == 4)
        {
            decoded[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                decoded[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                decoded[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    decoded[p] = '\0';
    return decoded;
}

void *thread_decode(void *arg)
{
    pthread_t id = pthread_self();
    char path1[10];
    char path2[12];
    char filename[10];
    if (pthread_equal(id, tid1[1]))
    {
        strcpy(path1, "./music/");
        strcpy(path2, "./music/%s");
        strcpy(filename, "music.txt");
    }
    else
    {
        strcpy(path1, "./quote/");
        strcpy(path2, "./quote/%s");
        strcpy(filename, "quote.txt");
    }
    DIR *dir;
    struct dirent *dp;
    dir = opendir(path1);
    FILE *file, *txt;

    if (dir != NULL)
    {
        while ((dp = readdir(dir)))
        {
            char str[100] = "";
            char fullpath[1000] = "";
            if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                sprintf(fullpath, path2, dp->d_name);
                file = fopen(fullpath, "r");
                while (fgets(str, 100, file) != NULL)
                {
                    txt = fopen(filename, "a");
                    fprintf(txt, "%s\n", base64_decode(str));
                    fclose(txt);
                }
                fclose(file);
            }
        }
        (void)closedir(dir);
    }
    else
        perror("Cannot open directory");
    return NULL;
}
void unzip(){
  pid_t child;
  child = fork();
  if(fork() == 0){
    char *unzip[5] = {"unzip", "-P","mihinomenestamanda", "hasil.zip",  NULL};
	execv ("/usr/bin/unzip",unzip);
  }else {
    printf("close\n");
	return;
  }
}

void* notxt(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid2[0]))
	{
		unzip();
	}
	else if(pthread_equal(id,tid2[1]))
	{	
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL)
		{
			printf("Error opening the file %s", filename);
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	}
	return NULL;
}
