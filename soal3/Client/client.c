#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define PORT 8080

char folder_directory[100] = "hartakarun";

int sending(int socket, char *filename);
int createConnection(char *filename);
void zipFile()


int main(int argc, char const *argv[])
{   
    zipFile();
    return 0;
}

int sending(int socket, char *filename)
{
    // Inisialiasi
    char fpath[1024], buffer[1024] = {0};
    int sizeFile;

    // Directory - File Name
    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *file = fopen(fpath, "r");
    
    // Checking 
    if (file != NULL)
    {
        bufferOnset(buffer, 1024);
    } else {
        printf("File tidak ditemukan: %s\n", filename);
        return -1;
    }

    // Hitung ukuran File
    sizeFile = fread(buffer, sizeof(char)

    // Lakukan perulangan
    while ((sizeFile, 1024, file)) > 0)
    {
        if (send(socket, buffer, sizeFile, 0) < 0)
        {
            fprintf(stderr, "Gagal mengirimkan file: %s.\n", filename);
            break;
        }
        (buffer, 1024);
    }
    fclose(file);
    return 0;
}

int createConnection(char *filename){
    // Inisialisasi
    struct sockaddr_in address, serverAddress;
    int sock = 0, valread;
    char *hello = "Request dari Client", buffer[1024] = {0};

    // Check Socket
    socketCheck = socket(AF_INET, SOCK_STREAM, 0)
    if (socketCheck < 0) {
        printf("\n Membuat Socket Error \n");
        return -1;
    }

    memset(&serverAddress, '0', sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    // Check Koneksi
    if (connect(socketCheck, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        printf("\nKoneksi Gagal!\n");
        return -1;
    }

    // Kirim File
    send(socketCheck , hello , strlen(hello) , 0 );
    sending(sock, filename);
    printf("\nKirim File Berhasil\n");

    return 0;
}

void zipFile(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        int status = 0;
        waitpid(child_id, &status, 0);
    }

    if (child_id == 0) {
        char *argv[]={"zip","-mr","hartakarun.zip","/soal3/hartakarun", NULL};
        execv("/usr/bin/zip",argv);
    } else {
        while ((wait(&status)) > 0);
        createConnection("hartakarun.zip");
    }
    
}