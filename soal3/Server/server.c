#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define PORT 8080

int receiver(int socket, char *filename);


int main(int argc, char const *argv[]) {
    //Inisiasi
    int server, newSocket, value, index = 1;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    // Checking
    server = socket(AF_INET, SOCK_STREAM, 0)

    if (server == 0) {
        perror("Socket Error");
        exit(EXIT_FAILURE);
    }

    // Error set socket
    if (setsockopt(server, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Address Inisiaslisasi
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    // Bin Failled
    int binds = bind(server, (struct sockaddr *)&address, sizeof(address));
    if (binds< 0) {
        perror("Bind failed");
        exit(EXIT_FAILURE);
    }

    // Listen Status
    if (listen(server, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    newSocket = accept(server, (struct sockaddr *)&address, (socklen_t*)&addrlen);
    if (newSocket < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    
    value = read( newSocket , buffer, 1024);

    receiver(newSocket, "hartakarun.zip");
    close(newSocket);
    return 0;
}

int receiver(int socket, char *filename)
{
    char buffer[1024] = {0}, fpath[1024];
    int sizeFile = 0;
    // Directory - File Name
    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *read_file = fopen(fpath, "wb");

    // Checking
    if (read_file == NULL)
    {
        printf("File tidak ditemukan: %s\n", filename);
        return -1;
    }
    else {
        printf("Dapat File dari Client: %s\n", filename);
        bufferOnset(buffer, 1024);
        sizeFile = recv(socket, buffer, 1024, 0);

        //menerima file dari client
        while (sizeFile > 0)
        {
            int tulisUkuran = fwrite(buffer, sizeof(char), sizeFile, read_file);
            if (tulisUkuran < sizeFile)
            {
                printf("Server Gagal Menyimpan\n");
                return 0;
            }

            bufferOnset(buffer, 1024);

            if (sizeFile == 0)
            {
                break;
            }
        }
    }
    fclose(read_file);
}
