#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <ctype.h>
#include <zip.h>
#include <string.h>
#include "unzip.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define dir_delimter '/'
#define MAX_FILENAME 512
#define READ_SIZE 8192

pthread_t threads[5];
pid_t child;
int signal = 0;

void unzip_file(char *path)
{
    pid_t child_id = fork();
    if(child_id == 0)
    {
        execl("/usr/bin/unzip", "/usr/bin/unzip", path, NULL);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid(child_id, &status, 0);
    }
}

// Ref: playandcount
void *keeper(void *arg)
{
    char property[500], iter, *temp_arr[100], filename[500], temp_arg[100];
    unsigned long i = 0;
    pthread_t id = pthread_self();
}

bool checkFile(const char *file_nfilenameame)
{
    struct stat buffer;
    int check = stat(filename, &buffer);
    if (check == 0)
    {
        return true;
    } else 
    {
        return false;
    }
}

void *changeFile(void *directory)
{
    char dir[PATH_MAX];
    strcat(dir, "/hartakarun");

    char *filename = strrchr(directory, '/'), directoryCurrent[1000], directoryNew[1000], file[1000], directoryDefault[1000];
    
    if (strrchr(directory, '/')[1] == '.')
    {
        strcpy(directoryCurrent, "Hidden");
    } else if (strstr(directory, ".") != NULL)
    {
        strcpy(file, directory);
        strtok(file, ".");
        char *token = strtok(NULL, "");

        for (int i = 0; token[i]; i++)
        {
            token[i] = tolower(token[i]);
        }
        strcpy(directoryCurrent, token);
    }
    else 
    {
        strcpy(directoryCurrent, "Unknown");
    }

    //mengecek apakah file ada
    int checking = isFilechecking(directory);
    if (checking){
        snprintf(directoryDefault, 1000, "%s/%s",folder_directory, directoryCurrent); 
        mkdir(directoryDefault);
    }
    if (getCurrentDirectory(dir, sizeof(dir)) != NULL){
        snprintf(directoryNew, 1000, "%s/%s/%s%s", dir, folder_directory, directoryCurrent, filename); 
        rename(directory, directoryNew);
    }
}


void getting(char *directory)
{
    char path[1000];
    struct dirent *readFile;
    struct stat buffer;
    DIR *directoryCurrent = opendir(directory);

    if (!directoryCurrent){ 
        return;
    }

    readFile = readdir(directoryCurrent);

    while ( readFile != NULL)
    {
        if(strcmp(readFile->d_name, ".") != 0 && strcmp(readFile->d_name, "..") != 0)
        {
            snprintf(path, 1000, "%s/%s", directory, readFile->d_name); 

            if (stat(path, &buffer) == 0)
            {
                pthread_t thread;
                pthread_create(&thread, NULL, changeFile, (void *)path);
                pthread_join(thread, 0);
            }

            if (S_ISREG(buffer.st_mode))
            {
                pthread_t thread;
                pthread_create(&thread, NULL, changeFile, (void *)path);
                pthread_join(thread, NULL);
            }

            getting(path);
        }
    }
    closedir(directoryCurrent);
}

int main(int argc, char **argv) 
{
    // Unzip With Exec
    unzip_file("../hartakarun.zip");

    // Unzip File Batas ------------------------

    char *path = "hartakarun.zip";
    if ( argc < 2 )
    {
        printf( "usage:\n%s {file to unzip}\n", argv[ 0 ] );
        return -1;
    }

    // Open the zip file
    unzFile *zipfile = unzOpen( path );
    if ( zipfile == NULL )
    {
        printf( "%s: not found\n" );
        return -1;
    }

    // Get info about the zip file
    unz_global_info global_info;
    if ( unzGetGlobalInfo( zipfile, &global_info ) != UNZ_OK )
    {
        printf( "could not read file global info\n" );
        unzClose( zipfile );
        return -1;
    }

    // Buffer to hold data read from the zip file.
    char read_buffer[ READ_SIZE ];

    // Loop to extract all files
    uLong i;
    for ( i = 0; i < global_info.number_entry; ++i )
    {
        // Get info about current file.
        unz_file_info file_info;
        char filename[ MAX_FILENAME ] = "../hartakarun.zip";
        if ( unzGetCurrentFileInfo(
            zipfile,
            &file_info,
            filename,
            MAX_FILENAME,
            NULL, 0, NULL, 0 ) != UNZ_OK )
        {
            printf( "could not read file info\n" );
            unzClose( zipfile );
            return -1;
        }

        // Check if this entry is a directory or file.
        const size_t filename_length = strlen( filename );
        if ( filename[ filename_length-1 ] == dir_delimter )
        {
            // Entry is a directory, so create it.
            printf( "dir:%s\n", filename );
            //mkdir( "hartakarun" );
        }
        else
        {
            // Entry is a file, so extract it.
            printf( "file:%s\n", filename );
            if ( unzOpenCurrentFile( zipfile ) != UNZ_OK )
            {
                printf( "could not open file\n" );
                unzClose( zipfile );
                return -1;
            }

            // Open a file to write out the data.
            FILE *out = fopen( filename, "wb" );
            if ( out == NULL )
            {
                printf( "could not open destination file\n" );
                unzCloseCurrentFile( zipfile );
                unzClose( zipfile );
                return -1;
            }

            int error = UNZ_OK;
            do    
            {
                error = unzReadCurrentFile( zipfile, read_buffer, READ_SIZE );
                if ( error < 0 )
                {
                    printf( "error %d\n", error );
                    unzCloseCurrentFile( zipfile );
                    unzClose( zipfile );
                    return -1;
                }

                // Write data to file.
                if ( error > 0 )
                {
                    fwrite( read_buffer, error, 1, out ); // You should check return of fwrite...
                }
            } while ( error > 0 );

            fclose( out );
        }

        unzCloseCurrentFile( zipfile );

        // Go the the next entry listed in the zip file.
        if ( ( i+1 ) < global_info.number_entry )
        {
            if ( unzGoToNextFile( zipfile ) != UNZ_OK )
            {
                printf( "cound not read next file\n" );
                unzClose( zipfile );
                return -1;
            }
        }
    }

    unzClose( zipfile );

    // --------------------------- Unzip File Batas

    // Thread

    for(int i = 2; i < argc; i++)
    {
        int err;
        int signal = 1;
        err = pthread_create(&(threads[i]), NULL)
    }

    char currentDirectory[PATH_MAX];
    if (getCurrentDirectory(currentDirectory, sizeof(currentDirectory)) != NULL)
    {
        strcat(currentDirectory, "/");
        strcat(currentDirectory, folder_directory);
        getting(currentDirectory);
    }

    return 0;
}