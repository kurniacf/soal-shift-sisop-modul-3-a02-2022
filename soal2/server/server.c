#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#define PORT 8080

#define DESTINATION_FOLDER "/home/avind/shift3/soal3/server"
#define SOURCE_FOLDER "/home/avind/shift3/soal3/client" 


int make_user_file()
{
	FILE* file_ptr = fopen("user.txt", "a");
    fclose(file_ptr);
    return 0;
}

int make_problem_file()
{
	FILE* file_ptr = fopen("problem.tsv", "a");
    fclose(file_ptr);
    return 0;
}
    
int register_user(char* user, char* pass)
{
	FILE* file_ptr = fopen("user.txt", "a");
	fprintf(file_ptr,"%s:%s\n", user, pass);
	
    fclose(file_ptr);
    return 0;
}

int register_problem(char* user, char* judul)
{
	FILE* file_ptr = fopen("uproblem.tsv", "a");
	fprintf(file_ptr,"%s\t%s\n", judul, user);
	
    fclose(file_ptr);
    return 0;
}

int valid_user(char* user, int len)
{
	FILE* file_ptr = fopen("user.txt", "r");
	char line[100];
	while(fgets(line, sizeof(line), file_ptr))
	{
		if(strncmp(line, user, len)==0)	return 1;
	}
	return 0;
}

int valid_pass(char* pass, int len)
{
	if(len < 6)	return 0;
	int low=0,up=0,dig=0;
	char *s;
	for ( s = pass; *s; ++s )
    {
        if (!isupper((unsigned char)*s))	up=1;
        if (!isdigit((unsigned char)*s))	dig=1;
        if (!islower((unsigned char)*s))	low=1;
    }
    //printf("%d %d %d\n", up,dig,low);
    if(up+dig+low==3)	return 1;
	return 0;
}

int login_check(char* user, char* pass)
{
	FILE* file_ptr = fopen("user.txt", "r");
	char line[100];
	char user_info[2][20];
	int cor_user=0, i=0;
	while(fgets(line, sizeof(line), file_ptr))
	{
		char* token = strtok(line, ":");
		while (token != NULL) {
		    strcpy(user_info[i], token);
		    token = strtok(NULL, ":");
		    i++;
		}
		if(strcmp(user_info[0], user)==0)
	    {
	    	if(strncmp(user_info[1], pass, strlen(pass))==0)	cor_user=1;
	    }
	    i=0;
	}
	return cor_user;



}

int main(int argc, char const* argv[])
{
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char buffer[1024] = { 0 };

	// Creating socket file descriptor
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0))
		== 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	// Forcefully attaching socket to the port 8080
	if (setsockopt(server_fd, SOL_SOCKET,
				SO_REUSEADDR | SO_REUSEPORT, &opt,
				sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	// Forcefully attaching socket to the port 8080
	if (bind(server_fd, (struct sockaddr*)&address,
			sizeof(address))
		< 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	if (listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}
	
	make_user_file();
	make_problem_file();
	char *respond=(char*)malloc(100);
	do
	{	
		new_socket = accept(server_fd, (struct sockaddr*)&address,
				(socklen_t*)&addrlen);
		sleep(2);
		int logged_status = 0;
		char* cur_user;
		memset(buffer,0,strlen(buffer));
		read(new_socket, buffer, 1024);
		//printf("%s\n", buffer);
		if(strncmp(buffer, "register", 8)==0)
		{
			char userid[20];
			char userpass[20];
			do{
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "insert new user:");
				send(new_socket, respond, strlen(respond), 0);
				read(new_socket, buffer, 1024);
				if(valid_user(buffer, strlen(buffer)))
				{
					memset(respond,0,strlen(respond));
					strcpy(respond, "User already exist.\n");
					send(new_socket, respond, strlen(respond), 0);
				}
			}while(valid_user(buffer, strlen(buffer)));
			
			strcpy(userid, buffer);
			do{
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "insert new pass:");
				send(new_socket, respond, strlen(respond), 0);
				read(new_socket, buffer, 1024);
				int check_pass = valid_pass(buffer, strlen(buffer));
				
				if(!check_pass)
				{
					memset(respond,0,strlen(respond));
					strcpy(respond, "Password must contain atleast 6 character, a number, a lowercase, and an uppercase.\n");
					send(new_socket, respond, strlen(respond), 0);
				}
			}while(!valid_pass(buffer, strlen(buffer)));
			
			strcpy(userpass, buffer);
			register_user(userid, userpass);
			memset(respond,0,strlen(respond));
			strcpy(respond, "User registered.\n");
			send(new_socket, respond, strlen(respond), 0);
		}
		
		
		else if(strncmp(buffer, "login", 5)==0)
		{
			char logid[20];
			char logpass[20];
			do{
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "insert user:");
				send(new_socket, respond, strlen(respond), 0);
				read(new_socket, buffer, 1024);
				if(!valid_user(buffer, strlen(buffer)))
				{
					memset(respond,0,strlen(respond));
					strcpy(respond, "User does not exist.\n");
					send(new_socket, respond, strlen(respond), 0);
				}
			}while(!valid_user(buffer, strlen(buffer)));
			
			strcpy(logid, buffer);
			
			do{
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "insert pass:");
				send(new_socket, respond, strlen(respond), 0);
				read(new_socket, buffer, 1024);
				int loginc = login_check(logid, buffer);
				if(!loginc)
				{
					sleep(2);
					memset(respond,0,strlen(respond));
					strcpy(respond, "Wrong password.\n");
					send(new_socket, respond, strlen(respond), 0);
				}
			}while(!login_check(logid, buffer));
			logged_status=1;
			strcpy(cur_user, logid);
		}
		
		if(logged_status)
		{
			char login_note[100] = "Currently logged in as ";
			strcat(login_note, cur_user);
			strcat(login_note, "\n");
			send(new_socket, login_note, strlen(login_note), 0);
			memset(buffer,0,strlen(buffer));
			read(new_socket, buffer, 1024);
			
			if(strncmp(buffer, "add", 3)==0)
			{
				char problem_info[4][50];
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "Judul problem:");
				send(new_socket, respond, strlen(respond), 0);
				problem_info[0] = read(new_socket, buffer, 1024);
				register_problem(problem_info[0], cur_user);
				
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "Filepath description.txt:");
				send(new_socket, respond, strlen(respond), 0);
				problem_info[1] = read(new_socket, buffer, 1024);
				
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "Filepath input.txt:");
				send(new_socket, respond, strlen(respond), 0);
				problem_info[2] = read(new_socket, buffer, 1024);
				
				memset(buffer,0,strlen(buffer));
				memset(respond,0,strlen(respond));
				strcpy(respond, "Filepath output.txt:");
				send(new_socket, respond, strlen(respond), 0);
				problem_info[3] = read(new_socket, buffer, 1024);
			
			}
			
			else if(strncmp(buffer, "see", 3)==0)
			{
			
			}
			
			else if(strncmp(buffer, "download", 8)==0)
			{
			
			}
			
			else if(strncmp(buffer, "submit", 6)==0)
			{
			
			}
			
			
		}
		memset(respond,0,strlen(respond));
		strcpy(respond, "user is done.\n");
		send(new_socket, respond, strlen(respond), 0);
		printf("Hello message sent.");
		close(new_socket);
	}while(1);
	return 0;
}

