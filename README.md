# Sistem Operasi 2022
Kelas Sistem Operasi A - Kelompok A02
### Nama Anggota 
- Kurnia Cahya Febryanto (5025201073)
- Amanda Salwa Salsabila (5025201172)
- Avind Pramana Azhari (05111940000226)

## Kendala
### Soal 1
Baru hanya berhasil mengerjakan nomor 1a. Tidak dapat mendecode file txt dalam zip untuk nomor 1b. Tidak dapat membuat folder hasil dan memindahkan hasil decode file txt ke dalam folder hasil tersebut untuk nomor 1c. Belum mencoba nomor 1d 

## Shift 3
### Soal 1
Dalam fungsi main, folder 'hasil' dibuat pertama kali menggunakan perintah `execv` `mkdir`.
```
if(fork() == 0){
		char *argv[] = {"mkdir", "-p", "hasil", NULL};	
		execv("/usr/bin/mkdir", argv);
	  }
while(wait(&status) > 0);
```

#### 1a
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
#### Pembahasan
Pembuatan dir, download dan unzip dilakukan dalam thread download_unzip, dimana terdapat 2 tid, untuk quote dan music, yang masing-masing memiliki 1 child untuk mkdir, download, dan unzip file. Mkdir, download dan unzip dilakukan dengan menggunakan `execv` `mkdir`.
```
void* download_unzip(void *arg){
  unsigned long i=0;
	pthread_t id=pthread_self();
	int iter, status;

    char *url[] = {"https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download",
		           "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download"};
	
    char *fileName[] = {"quote.zip","music.zip"};

    char *folder[] = {"quote","music"};

    if(pthread_equal(id, tid[0])){
		if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[0], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[0], "-O", fileName[0], NULL};
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[0], "-d", "/home/amanda/quote", NULL};	
			execv("/usr/bin/unzip", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[0], NULL};
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);

    }else if(pthread_equal(id, tid[1])){
         if(fork() == 0){
			char *argv[] = {"mkdir", "-p", folder[1], NULL};
			execv("/usr/bin/mkdir", argv);
		}
		while (wait(&status) > 0);
	
		if(fork() == 0){
			char *argv[] = {"wget", "-q", "--no-check-certificate", url[1], "-O", fileName[1], NULL};		
			execv("/usr/bin/wget", argv);
		}
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"unzip", fileName[1], "-d", "/home/amanda/music", NULL};
			execv("/usr/bin/unzip", argv);
		}	
		while (wait(&status) > 0);
		
		if(fork() == 0){
			char *argv[] = {"rm", fileName[1], NULL};	
			execv("/usr/bin/rm", argv);
		}
		while (wait(&status) > 0);
    }
    return NULL;
}
```
Thread ini dibuat dan dipanggil di main
```
while(i < 2) 
	{
		err=pthread_create(&(tid[i]),NULL,&download_unzip,NULL); 
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```
#### 1b
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
#### Pembahasan
Untuk mendecode base64, diperlukan inisialisasi map.
```
char base64_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
```
Decode file .txt dilakukan dengan menggunakan perulangan `for` dalam fungsi thread base64_decode.
```
char *base64_decode(char *encoded)
{
    char counts = 0;
    char buffer[4];
    char *decoded = malloc(strlen(encoded) * 3 / 4);
    int i = 0, p = 0;

    for (i = 0; encoded[i] != '\0'; i++)
    {
        char k;
        for (k = 0; k < 64 && base64_map[k] != encoded[i]; k++)
            ;
        buffer[counts++] = k;
        if (counts == 4)
        {
            decoded[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if (buffer[2] != 64)
                decoded[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if (buffer[3] != 64)
                decoded[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }
    decoded[p] = '\0';
    return decoded;
}
```
Thread ini dibuat dan dipanggil di main
```
while (j < 2)
    {
        err = pthread_create(&(tid1[j]), NULL, &thread_decode, NULL);
        j++;
    }
    pthread_join(tid1[0], NULL);
    pthread_join(tid1[1], NULL);
```
#### 1c
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
#### Pembahasan
Pemindahan kedua file .txt dilakukan menggunakan `execv mv` di dalam fungsi main.
```
if(fork() == 0){
    char *argv[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
    while(wait(&status) > 0);

    if (fork() == 0)
    {
        char *argv[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
        execv("/usr/bin/mv", argv);
    }
```
#### 1d
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. 
#### Pembahasan
File di-zip menggunakan perintah `execv zip` dan `-rmvP` agar zip bisa diberi password. Proses ini dilakukan langsung di fungsi main.
```
if(fork()==0){
		char *argv[] = {"zip", "-rmvqP", "mihinomenestamanda", "-r", "hasil", "hasil",NULL}; 
		execv("/usr/bin/zip", argv);
	}while(wait(&status) > 0);
	
	for(int i = 0; i < 2; i++){	
	if(fork() == 0){
		char *argv[] = {"rm", "-rf", folder[i], NULL};
		execv("/usr/bin/rm", argv);
	}
	}
```
#### 1e
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
#### Pembahasan
Pembuatan file no.txt dilakukan dengan menggunakan fungsi thread notxt dengan menggunakan 2 thread id, dimana thread id pertama gunanya untuk melakukan penulisan kata 'No' pada saat yang bersamaan
```
void* notxt(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid2[0]))
	{
		unzip();
	}
	else if(pthread_equal(id,tid2[1]))
	{	
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL)
		{
			printf("Error opening the file %s", filename);
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	}
	return NULL;
}
```
Fungsi ini dibuat dan dipanggil di main
```
while (k < 2)
    {
        err = pthread_create(&(tid2[k]), NULL, &notxt, NULL);
        k++;
    }
    pthread_join(tid2[0], NULL);
    pthread_join(tid2[1], NULL);
```
Zip kedua file, yakni file hasil dan file no.txt ke dalam satu file yang bernama hasil.zip dilakukan di dalam fungsi main
```
 if(fork() == 0){
    char *argv[] = {"mv", "no.txt", "hasil/no.txt", NULL};
        execv("/usr/bin/mv", argv);
    }

    if(fork()==0){
        char *args[] = {"zip", "-q","hasil.zip", "-r","--password", "mihinomenestamanda", "hasil/", "no.txt", NULL}; 
        execv("/usr/bin/zip", args);
        }while(wait(&status) > 0);
```

### Soal 2
### Soal 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.
Contoh jika program pengkategorian dijalankan

#### 3-a
Dilakukan unzip file dengan menggunakan library unzip.h dan zlib. 
Library yang digunakan yaitu
```
#include <zip.h>
#include "unzip.h"
```
Langkah pertama yaitu memilih path dan menentukan apakah bisa untuk di unzip atau tidak.
```
char *path = "hartakarun.zip";
    if ( argc < 2 )
    {
        printf( "usage:\n%s {file to unzip}\n", argv[ 0 ] );
        return -1;
    }
```
Kemudian membuka file Zip dengan menggunakan bantuan library unzip.h
```
unzFile *zipfile = unzOpen( path );
if ( zipfile == NULL )
{
    printf( "%s: not found\n" );
    return -1;
}
```
Cari informasi global dari zip digunakan untuk mengecek apakah ada kredensial atau tidak.
```
unz_global_info global_info;
if ( unzGetGlobalInfo( zipfile, &global_info ) != UNZ_OK )
{
    printf( "could not read file global info\n" );
    unzClose( zipfile );
    return -1;
}
```
Lalu di buffer dan dilakukan extract file 
```
char read_buffer[ READ_SIZE ];

uLong i;
for ( i = 0; i < global_info.number_entry; ++i )
{
    // Get info about current file.
    unz_file_info file_info;
    char filename[ MAX_FILENAME ] = "../hartakarun.zip";
    if ( unzGetCurrentFileInfo(
        zipfile,
        &file_info,
        filename,
        MAX_FILENAME,
        NULL, 0, NULL, 0 ) != UNZ_OK )
    {
        printf( "could not read file info\n" );
        unzClose( zipfile );
        return -1;
    }

    // Check if this entry is a directory or file.
    const size_t filename_length = strlen( filename );
    if ( filename[ filename_length-1 ] == dir_delimter )
    {
        // Entry is a directory, so create it.
        printf( "dir:%s\n", filename );
        //mkdir( "hartakarun" );
    }
    else
    {
        // Entry is a file, so extract it.
        printf( "file:%s\n", filename );
        if ( unzOpenCurrentFile( zipfile ) != UNZ_OK )
        {
            printf( "could not open file\n" );
            unzClose( zipfile );
            return -1;
        }

        // Open a file to write out the data.
        FILE *out = fopen( filename, "wb" );
        if ( out == NULL )
        {
            printf( "could not open destination file\n" );
            unzCloseCurrentFile( zipfile );
            unzClose( zipfile );
            return -1;
        }

        int error = UNZ_OK;
        do    
        {
            error = unzReadCurrentFile( zipfile, read_buffer, READ_SIZE );
            if ( error < 0 )
            {
                printf( "error %d\n", error );
                unzCloseCurrentFile( zipfile );
                unzClose( zipfile );
                return -1;
            }

            // Write data to file.
            if ( error > 0 )
            {
                fwrite( read_buffer, error, 1, out ); // You should check return of fwrite...
            }
        } while ( error > 0 );

        fclose( out );
    }

```
Tutup hasil extract zip dan urutkan hasil ekstrak sesuai read zip yang didapatkan
```
unzCloseCurrentFile( zipfile );
if ( ( i+1 ) < global_info.number_entry )
{
    if ( unzGoToNextFile( zipfile ) != UNZ_OK )
    {
        printf( "cound not read next file\n" );
        unzClose( zipfile );
        return -1;
    }
}
unzClose( zipfile );
```

</br>

#### 3-b
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.
Langkah awal yaitu menentukan directory untuk hartakarun
```
char dir[PATH_MAX];
strcat(dir, "/hartakarun");
char *filename = strrchr(directory, '/'), directoryCurrent[1000], directoryNew[1000], file[1000], directoryDefault[1000];
```
Lalu dilakukan pengecekan menggunakan `strstr` untuk file yang masuk kategori hidden yaitu
```
if (strrchr(directory, '/')[1] == '.')
    strcpy(directoryCurrent, "Hidden")
``` 
Untuk yang masuk kategori unknown atau tidak memiliki ekstensi maka 
```
strcpy(directoryCurrent, "Unknown")
```
Untuk yang memiliki directory lengkap menjadi sebuah file maka dilakukan pencarian Token untuk penyesuaian nama file dan ekstensi.
```
strcpy(file, directory);
strtok(file, ".");
char *token = strtok(NULL, "");

for (int i = 0; token[i]; i++)
{
    token[i] = tolower(token[i]);
}
strcpy(directoryCurrent, token);
```

</br>

#### 3-c
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.
Untuk dapat berjalan dan dioperasikan dalam 1 thread maka dilakukan pengecekan yaitu sebagai berikut
```
 if (stat(path, &buffer) == 0)
{
    pthread_t thread;
    pthread_create(&thread, NULL, changeFile, (void *)path);
    pthread_join(thread, 0);
}
```
```
if (S_ISREG(buffer.st_mode))
{
    pthread_t thread;
    pthread_create(&thread, NULL, changeFile, (void *)path);
    pthread_join(thread, NULL);
}
```

jangan lupa sebelum itu harus melakukan penyesuaian nama dengan directorynya
```
snprintf(path, 1000, "%s/%s", directory, readFile->d_name); 
```

</br>

#### 3-d
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
Dapat dilakukan dengan menggunakan bantuan `execv` untuk zip nya

```
char *argv[]={"zip","-mr","hartakarun.zip","/soal3/hartakarun", NULL};
execv("/usr/bin/zip",argv);
```
Lalu jika status ada, maka akan melakukan create connection terhadap file zipnya. yang akan berkaitan dengan nomor 3-e
```
createConnection("hartakarun.zip");
```

</br>

#### 3-e
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server
`send hartakarun.zip`

Langkah pertama yaitu memperhatikan portnya apakah sesuai atau tidak
```
if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr)<=0) {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
}
```
Jangan lupa membuat socket
```
socketCheck = socket(AF_INET, SOCK_STREAM, 0)
if (socketCheck < 0) --> buat socket
```
Setelah itu, cek koneksi apakah sukses atau gagal
```
if (connect(socketCheck, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
```
Lalu kirim menggunakan fungsi send
```
send(socketCheck , hello , strlen(hello) , 0 );
sending(sock, filename);
```
Fungsi send yaitu fungsi yang digunakna untuk mengirimkan file
```
int sending(int socket, char *filename)
{
    // Inisialiasi
    char fpath[1024], buffer[1024] = {0};
    int sizeFile;

    // Directory - File Name
    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *file = fopen(fpath, "r");
    
    // Checking 
    if (file != NULL)
    {
        bufferOnset(buffer, 1024);
    } else {
        printf("File tidak ditemukan: %s\n", filename);
        return -1;
    }

    // Hitung ukuran File
    sizeFile = fread(buffer, sizeof(char)

    // Lakukan perulangan
    while ((sizeFile, 1024, file)) > 0)
    {
        if (send(socket, buffer, sizeFile, 0) < 0)
        {
            fprintf(stderr, "Gagal mengirimkan file: %s.\n", filename);
            break;
        }
        (buffer, 1024);
    }
    fclose(file);
    return 0;
}
```
